import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;
import java.util.StringTokenizer;

public class DoubleStack {
	// private LinkeListi, StringTokeneiseri ja equals kasutatud nagu loengus
	// soovitatud
	// http://enos.itcollege.ee/~jpoial/algoritmid/adt.html
	// http://enos.itcollege.ee/~jpoial/java/naited/Arv.java
	// http://docs.oracle.com/javase/6/docs/api/index.html

	private LinkedList<Double> list; // uus list

	public static void main(String[] argum) {

	}

	// konstruktor
	DoubleStack() {
		list = new LinkedList<Double>();
	}

	// võtame viimase ja lükkame uue listi sisse
	@Override
	public Object clone() throws CloneNotSupportedException {
		DoubleStack uuslist = new DoubleStack();
		for (int i = list.size() - 1; i > -1; i--) {
			uuslist.push(list.get(i));
		}
		return uuslist;
	}

	// kontrollib kas list on tühi
	public boolean stEmpty() {
		return list.isEmpty();
	}

	// paneb kõige peale
	public void push(double a) {
		list.push(a);
	}

	// algul kontrollib, kas tühi ja siis võtab kõige pealmise elemendi
	public double pop() {
		if (list.isEmpty()) {
			throw new RuntimeException("List on tühi");
		} else {
			return list.pop();
		}
	}

	// listi suurus testmiseks,et liikseid elemente ei oleks
	public double suurus() {
		return list.size();
	}

	// testin kas on number -
	// http://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java
	public static boolean viimaneOnNumber(String token) {
		try {
			double d = Double.parseDouble(token);

		} catch (NumberFormatException nfe) {
			return true;
		}
		return false;

	}

	// teeb vajalikud tehted, arv.java-st võetud
	// topelt kontroll pole paha, kontrollib is.empthy-ga üle ja list.sizega- kas piisavalt elemente
	//kas stringis element on ja väljastab märgi, kui on vale.
	public void op(String s) {
		if (list.size() < 2) {
			throw new RuntimeException("Liiga vähe elemente, millega arvutada");
		} else {
			double op2 = pop();
			double op1 = pop();

			if (s.isEmpty()) {
				throw new RuntimeException("List on tühi");
			} else {
				if (s.equals("+")) {
					push(op1 + op2);
				}
				if (s.equals("-")) {
					push(op1 - op2);
				}
				if (s.equals("*")) {
					push(op1 * op2);
				}
				if (s.equals("/")) {
					push(op1 / op2);
				} else {
					new RuntimeException("Vale tehte märk: " + s);
				}
			}
		}

	}

	// algul kontrollib kas on tühi ja siis vaatab kõige pealmist elementi
	public double tos() {
		if (list.isEmpty()) {
			throw new RuntimeException("List on tühi");
		} else {
			return list.peekFirst();
		}
	}

	// võrdleb
	// võtab DoubleStack object o-st listi välja ja võrdleb.
	@Override
	public boolean equals(Object o) {
		if (list.equals(((DoubleStack) o).list)) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * uus meetod
	 */
	@Override
	public String toString() {
		  StringBuffer uus = new StringBuffer();
		  for (int i = list.size() - 1; i >-1; i--){
			  uus.append(String.valueOf(list.get(i)) + " ");
		  }
		  return uus.toString();
	}

	/*
	 * int countTokens() Calculates the number of times that this tokenizer's
	 * nextToken method can be called before it generates an exception. boolean
	 * hasMoreElements() Returns the same value as the hasMoreTokens method.
	 * boolean hasMoreTokens() Tests if there are more tokens available from
	 * this tokenizer's string. Object nextElement() Returns the same value as
	 * the nextToken method, except that its declared return value is Object
	 * rather than String. String nextToken() Returns the next token from this
	 * string tokenizer. String nextToken(String delim) Returns the next token
	 * in this string tokenizer's string.
	 */

	// Täiendasin kahte veateadet. Annab välja avalise, kui ka põhjuse
	public static double interpret(String pol) {

		if (pol.isEmpty()) {
			throw new RuntimeException("String on tühi, ei ole millegiga tehteid teha");
		}
		StringTokenizer tokenid = new StringTokenizer(pol);
		DoubleStack vastus = new DoubleStack();
		while (tokenid.hasMoreTokens()) {
			String token = (String) tokenid.nextElement();
			if (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"))
				vastus.op(token);
			else if (viimaneOnNumber(token) == true) {
				throw new RuntimeException("Tehte " + pol + "Viimane element: " + token + " pole lubatud");
				// kui viimane on number, ei ole võimalik arvutada
			} else {
				vastus.push(Double.parseDouble(token)); // pane element tagasi
			}
		}

		// kui peale while tsüklid jääb üle ühe alles on vigane String
		if (vastus.suurus() > 1) {
			throw new RuntimeException(vastus.suurus() + "Tehtes: " + pol + "on vale arv elemente");
		}
		return vastus.tos();
	}

}
